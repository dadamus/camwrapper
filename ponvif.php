<?php

class Ponvif
{
    protected $ipaddress = '';

    protected $username = '';

    protected $password = '';

    protected $mediauri = '';

    protected $deviceuri = '';

    protected $ptzuri = '';

    protected $baseuri = '';

    protected $onvifversion = [];

    protected $deltatime = 0;

    protected $capabilities = [];

    protected $videosources = [];

    protected $sources = [];

    protected $profiles = [];

    protected $proxyhost = '';

    protected $proxyport = '';

    protected $proxyusername = '';

    protected $proxypassword = '';

    protected $lastresponse = '';

    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function setIPAddress(string $ipAddress)
    {
        $this->ipaddress = $ipAddress;
    }

    public function getSources(): array
    {
        return $this->sources;
    }

    /*
        Public functions (basic initialization method and other collaterals)
    */
    public function initialize()
    {
        $this->mediauri = 'http://' . $this->ipaddress . '/onvif/device_service';

        $datetime = $this->coreGetSystemDateAndTime();
        $timestamp = mktime($datetime['Time']['Hour'], $datetime['Time']['Minute'], $datetime['Time']['Second'],
            $datetime['Date']['Month'], $datetime['Date']['Day'], $datetime['Date']['Year']);
        $this->deltatime = time() - $timestamp - 5;

        $this->capabilities = $this->coreGetCapabilities();
        $onvifVersion = $this->getOnvifVersion($this->capabilities);
        $this->mediauri = $onvifVersion['media'];
        $this->deviceuri = $onvifVersion['device'];
        $this->ptzuri = $onvifVersion['ptz'];
        preg_match("/^http(.*)onvif\//", $this->mediauri, $matches);
        $this->baseuri = $matches[0];
        $this->onvifversion = array('major' => $onvifVersion['major'], 'minor' => $onvifVersion['minor']);
        $this->videosources = $this->mediaGetVideoSources();
        $this->profiles = $this->mediaGetProfiles();
        $this->sources = $this->getActiveSources($this->videosources, $this->profiles);
    }

    public function isFault($response)
    { // Useful to check if response contains a fault
        return array_key_exists('Fault', $response) || array_key_exists('Fault', $response['Envelope']['Body']);
    }

    /*
        Public wrappers for a subset of ONVIF primitives
    */
    public function coreGetSystemDateAndTime(): array
    {
        $post_string = '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><GetSystemDateAndTime xmlns="http://www.onvif.org/ver10/device/wsdl"/></s:Body></s:Envelope>';
        if ($this->isFault($response = $this->sendRequest($this->mediauri, $post_string))) {
            throw new Exception('GetSystemDateAndTime: Communication error');
        }

        return $response['Envelope']['Body']['GetSystemDateAndTimeResponse']['SystemDateAndTime']['UTCDateTime'];
    }

    public function coreGetCapabilities(): array
    {
        $REQ = $this->makeToken();
        $post_string = '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"><s:Header><Security s:mustUnderstand="1" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><UsernameToken><Username>%%USERNAME%%</Username><Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">%%PASSWORD%%</Password><Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">%%NONCE%%</Nonce><Created xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">%%CREATED%%</Created></UsernameToken></Security></s:Header><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><GetCapabilities xmlns="http://www.onvif.org/ver10/device/wsdl"><Category>All</Category></GetCapabilities></s:Body></s:Envelope>';
        $post_string = str_replace(
            [
                "%%USERNAME%%",
                "%%PASSWORD%%",
                "%%NONCE%%",
                "%%CREATED%%"
            ],
            [
                $REQ['USERNAME'],
                $REQ['PASSDIGEST'],
                $REQ['NONCE'],
                $REQ['TIMESTAMP']
            ],
            $post_string);
        if ($this->isFault($response = $this->sendRequest($this->mediauri, $post_string))) {
            throw new Exception('GetCapabilities: Communication error');
        }

        return $response['Envelope']['Body']['GetCapabilitiesResponse']['Capabilities'];
    }

    public function mediaGetVideoSources(): array
    {
        $REQ = $this->makeToken();
        $post_string = '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"><s:Header><Security s:mustUnderstand="1" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><UsernameToken><Username>%%USERNAME%%</Username><Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">%%PASSWORD%%</Password><Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">%%NONCE%%</Nonce><Created xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">%%CREATED%%</Created></UsernameToken></Security></s:Header><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><GetVideoSources xmlns="http://www.onvif.org/ver10/media/wsdl"/></s:Body></s:Envelope>';
        $post_string = str_replace(
            [
                "%%USERNAME%%",
                "%%PASSWORD%%",
                "%%NONCE%%",
                "%%CREATED%%"
            ],
            [
                $REQ['USERNAME'],
                $REQ['PASSDIGEST'],
                $REQ['NONCE'],
                $REQ['TIMESTAMP']
            ],
            $post_string
        );
        if ($this->isFault($response = $this->sendRequest($this->mediauri, $post_string))) {
            throw new Exception('GetVideoSources: Communication error');
        }

        return $response['Envelope']['Body']['GetVideoSourcesResponse']['VideoSources'];
    }

    public function mediaGetProfiles(): array
    {
        $REQ = $this->makeToken();
        $post_string = '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"><s:Header><Security s:mustUnderstand="1" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><UsernameToken><Username>%%USERNAME%%</Username><Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">%%PASSWORD%%</Password><Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">%%NONCE%%</Nonce><Created xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">%%CREATED%%</Created></UsernameToken></Security></s:Header><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><GetProfiles xmlns="http://www.onvif.org/ver10/media/wsdl"/></s:Body></s:Envelope>';
        $post_string = str_replace(
            [
                "%%USERNAME%%",
                "%%PASSWORD%%",
                "%%NONCE%%",
                "%%CREATED%%"
            ],
            [
                $REQ['USERNAME'],
                $REQ['PASSDIGEST'],
                $REQ['NONCE'],
                $REQ['TIMESTAMP']
            ],
            $post_string
        );
        if ($this->isFault($response = $this->sendRequest($this->mediauri, $post_string))) {
            throw new Exception('GetProfiles: Communication error');
        }

        return $response['Envelope']['Body']['GetProfilesResponse']['Profiles'];
    }

    public function mediaGetSnapshotUri($profileToken): string
    {
        $REQ = $this->makeToken();
        $post_string = '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"><s:Header><Security s:mustUnderstand="1" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><UsernameToken><Username>%%USERNAME%%</Username><Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">%%PASSWORD%%</Password><Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">%%NONCE%%</Nonce><Created xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">%%CREATED%%</Created></UsernameToken></Security></s:Header><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><GetSnapshotUri xmlns="http://www.onvif.org/ver10/media/wsdl"><ProfileToken>%%PROFILETOKEN%%</ProfileToken></GetSnapshotUri></s:Body></s:Envelope>';
        $post_string = str_replace(
            [
                "%%USERNAME%%",
                "%%PASSWORD%%",
                "%%NONCE%%",
                "%%CREATED%%",
                "%%PROFILETOKEN%%"
            ],
            [
                $REQ['USERNAME'],
                $REQ['PASSDIGEST'],
                $REQ['NONCE'],
                $REQ['TIMESTAMP'],
                $profileToken
            ],
            $post_string);
        if ($this->isFault($response = $this->sendRequest($this->mediauri, $post_string))) {
            throw new Exception('GetSnapshotUri: Communication error');
        }

        return $response['Envelope']['Body']['GetSnapshotUriResponse']['MediaUri']['Uri'];
    }

    /*
        Internal functions
    */
    protected function makeToken(): array
    {
        $timestamp = time() - $this->deltatime;

        return $this->passwordDigest($this->username, $this->password, date('Y-m-d\TH:i:s.000\Z', $timestamp));
    }

    protected function getOnvifVersion($capabilities): array
    {
        $version = array();
        if (isset($capabilities['Device']['System']['SupportedVersions']['Major'])) {
            // NVT supports a specific onvif version
            $version['major'] = $capabilities['Device']['System']['SupportedVersions']['Major'];
            $version['minor'] = $capabilities['Device']['System']['SupportedVersions']['Minor'];
        } else {
            // NVT supports more onvif versions
            $currentma = 0;
            $currentmi = 0;
            foreach ($capabilities['Device']['System']['SupportedVersions'] as $cver) {
                if ($cver['Major'] > $currentma) {
                    $currentma = $cver['Major'];
                    $currentmi = $cver['Minor'];
                }
            }
            $version['major'] = $currentma;
            $version['minor'] = $currentmi;
        }
        $version['media'] = $capabilities['Media']['XAddr'];
        $version['device'] = $capabilities['Device']['XAddr'];
        $version['event'] = $capabilities['Events']['XAddr'];
        if (isset($capabilities['PTZ']['XAddr'])) {
            $version['ptz'] = $capabilities['PTZ']['XAddr'];
        } else {
            $version['ptz'] = '';
        }

        return $version;
    }

    protected function getActiveSources($videoSources, $profiles): array
    {
        $sources = [];

        if (isset($videoSources['@attributes'])) {
            // NVT is a camera
            $sources[0]['sourcetoken'] = $videoSources['@attributes']['token'];
            $this->getProfileData($sources, 0, $profiles);
        } else {
            // NVT is an encoder
            for ($i = 0; $i < count($videoSources); $i++) {
                if (strtolower($videoSources[$i]['@attributes']['SignalActive']) == 'true') {
                    $sources[$i]['sourcetoken'] = $videoSources[$i]['@attributes']['token'];
                    $this->getProfileData($sources, $i, $profiles);
                }
            } // for
        }

        return $sources;
    }

    protected function getProfileData(&$sources, $i, $profiles)
    {
        $inProfile = 0;
        for ($y = 0; $y < count($profiles); $y++) {
            if (!isset($profiles[$y]['VideoSourceConfiguration'])) {
                continue;
            }

            if ($profiles[$y]['VideoSourceConfiguration']['SourceToken'] == $sources[$i]['sourcetoken']) {
                $sources[$i][$inProfile]['profilename'] = $profiles[$y]['Name'];
                $sources[$i][$inProfile]['profiletoken'] = $profiles[$y]['@attributes']['token'];

                if (isset($profiles[$y]['VideoEncoderConfiguration'])) {
                    $sources[$i][$inProfile]['encoding'] = $profiles[$y]['VideoEncoderConfiguration']['Encoding'];
                    $sources[$i][$inProfile]['width'] = $profiles[$y]['VideoEncoderConfiguration']['Resolution']['Width'];
                    $sources[$i][$inProfile]['height'] = $profiles[$y]['VideoEncoderConfiguration']['Resolution']['Height'];
                    $sources[$i][$inProfile]['fps'] = $profiles[$y]['VideoEncoderConfiguration']['RateControl']['FrameRateLimit'];
                    $sources[$i][$inProfile]['bitrate'] = $profiles[$y]['VideoEncoderConfiguration']['RateControl']['BitrateLimit'];
                }

                if (isset($profiles[$y]['PTZConfiguration'])) {
                    $sources[$i][$inProfile]['ptz']['name'] = $profiles[$y]['PTZConfiguration']['Name'];
                    $sources[$i][$inProfile]['ptz']['nodetoken'] = $profiles[$y]['PTZConfiguration']['NodeToken'];
                }

                $inProfile++;
            }
        }
    }

    protected function xml2array(string $response): array
    {
        $sxe = new SimpleXMLElement($response);
        $dom_sxe = dom_import_simplexml($sxe);
        $dom = new DOMDocument('1.0');
        $dom_sxe = $dom->importNode($dom_sxe, true);
        $dom_sxe = $dom->appendChild($dom_sxe);
        $element = $dom->childNodes->item(0);
        foreach ($sxe->getDocNamespaces() as $name => $uri) {
            $element->removeAttributeNS($uri, $name);
        }
        $xmldata = $dom->saveXML();
        $xmldata = substr($xmldata, strpos($xmldata, "<Envelope>"));
        $xmldata = substr($xmldata, 0, strpos($xmldata, "</Envelope>") + strlen("</Envelope>"));
        $xml = simplexml_load_string($xmldata);
        $data = json_decode(json_encode((array)$xml), 1);
        $data = [$xml->getName() => $data];

        return $data;
    }

    protected function passwordDigest(
        string $username,
        string $password,
        string $timestamp = "default",
        string $nonce = "default"
    ): array {
        if ($timestamp == 'default') $timestamp = date('Y-m-d\TH:i:s.000\Z');
        if ($nonce == 'default') $nonce = mt_rand();
        $REQ = array();
        $passdigest = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $timestamp) . pack('a*', $password))));
        //$passdigest=base64_encode(sha1($nonce.$timestamp.$password,true)); // alternative
        $REQ['USERNAME'] = $username;
        $REQ['PASSDIGEST'] = $passdigest;
        $REQ['NONCE'] = base64_encode(pack('H*', $nonce));
        //$REQ['NONCE']=base64_encode($nonce); // alternative
        $REQ['TIMESTAMP'] = $timestamp;

        return $REQ;
    }

    protected function sendRequest(string $url, string $post_string): array
    {
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        if ($this->proxyhost != '' && $this->proxyport != '') {
            curl_setopt($soap_do, CURLOPT_PROXY, $this->proxyhost);
            curl_setopt($soap_do, CURLOPT_PROXYPORT, $this->proxyport);
            if ($this->proxyusername != '' && $this->proxypassword != '') {
                curl_setopt($soap_do, CURLOPT_PROXYUSERPWD, $this->proxyusername . ':' . $this->proxypassword);
            }
        }
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, [
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
        ]);
        //curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password); // HTTP authentication
        if (($result = curl_exec($soap_do)) === false) {
            $err = curl_error($soap_do);
            $this->lastresponse = ["Fault" => $err];
        } else {
            $this->lastresponse = $this->xml2array($result);
        }

        return $this->lastresponse;
    }
}
