<?php

require_once __DIR__ . '/ponvif.php';

$connector = new Ponvif();
$connector->setIPAddress('192.168.1.');
$connector->setPassword('admin');
$connector->setUsername('admin');

$connector->initialize();
$sources = $connector->getSources();
$profileToken = $sources[0][0]['profiletoken'];
$imageUrl = $connector->mediaGetSnapshotUri($profileToken);
saveImage($imageUrl);

function saveImage(string $url)
{
    $image = getImageFromUrl($url);
    $imageName = date("Y-m-d-H:i:s") . '.jpg';

    $dir = __DIR__ . '/' . date('Y-m-d');
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true);
    }

    $saveFile = fopen(sprintf("%s/%s", $dir, $imageName), 'w');
    fwrite($saveFile, $image);
    fclose($saveFile);
}

function getImageFromUrl($link)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}
